package com.ming.liteflow.cmp.script;

import com.yomahub.liteflow.script.ScriptExecuteWrap;
import com.yomahub.liteflow.script.body.JaninoBreakScriptBody;

public class ScriptBreakCmp implements JaninoBreakScriptBody {
    @Override
    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {
        System.out.println("ScriptBreakCmp executed!");
        return true;
    }
}
