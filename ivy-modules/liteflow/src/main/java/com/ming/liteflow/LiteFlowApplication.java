package com.ming.liteflow;

import com.ming.common.generate.template.enable.EnableGenerate;
import com.ming.common.generate.template.scan.GenerateScan;
import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

//@ComponentScans(@ComponentScan(basePackages = "com.ming.common"))
//@EnableGenerate(basePackages = {"com.ming.liteflow"})
//@SpringBootApplication(exclude = {RedissonAutoConfiguration.class})
//public class LiteFlowApplication {
//    private static Logger log = LoggerFactory.getLogger(LiteFlowApplication.class);
//    public static void main(String[] args) {
//        SpringApplication.run(LiteFlowApplication.class, args);
//        log.info("LiteflowApplication 启动成功！");
//    }
//
//}
