package com.ming.liteflow.cmp.fallback;

import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeWhileComponent;
@FallbackCmp
@LiteflowComponent("WhileFallbackCmp")
public class WhileFallbackCmp extends NodeWhileComponent {
    @Override
    public boolean processWhile() throws Exception {
        System.out.println("WhileFallbackCmp executed!");
        return false;
    }
}