package com.ming.liteflow.controller;

import com.ming.common.beetl.util.Result;
import com.ming.common.beetl.util.StrUtil;
import com.ming.common.liteflow.core.config.IvyConfig;
import com.ming.common.liteflow.core.el.IvyEl;
import com.ming.common.liteflow.core.execption.LiteFlowELException;
import com.ming.common.util.ClassFieldUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.common.liteflow.core.flowexecutor.IvyExecutor;
import com.ming.common.liteflow.vo.IvyExecutorVo;
import com.ming.common.Options;
import com.ming.common.SortBy;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/liteflow/executor")
public class LiteFlowExecutorController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/option")
    @PermissionLimit(limit = false)
    public Result<?> option(@RequestBody Map<String,Object> map) throws LiteFlowELException {
        return Result.OK(ClassFieldUtil.getFieldAnnoDescribe(IvyExecutor.class, map));
    }

    @PostMapping("/options")
    @PermissionLimit(limit = false)
    public Result<?> options(@RequestBody(required = false) Map<String,Object> map) throws LiteFlowELException {
        LambdaQuery<IvyExecutor> query = sqlManager.lambdaQuery(IvyExecutor.class);
        List<IvyExecutor> list = query.select(IvyExecutor::getId,IvyExecutor::getExecutorId,IvyExecutor::getExecutorName);
        list = list.stream().peek(m->m.setExecutorName(m.getExecutorName()+"【"+m.getExecutorId()+"】")).collect(Collectors.toList());
        return Result.OK(list);
    }

    @PostMapping("/page")
    @PermissionLimit(limit = false)
    public Result<PageResult<IvyExecutor>> page(@RequestBody IvyExecutorVo vo){
        LambdaQuery<IvyExecutor> lambdaQuery = sqlManager.lambdaQuery(IvyExecutor.class);
        lambdaQuery.andLike(IvyExecutor::getExecutorId, LambdaQuery.filterLikeEmpty(vo.getExecutorId()));
        lambdaQuery.andLike(IvyExecutor::getExecutorName, LambdaQuery.filterLikeEmpty(vo.getExecutorName()));
        lambdaQuery.andEq(IvyExecutor::getIvyConfigId, LambdaQuery.filterEmpty(vo.getIvyConfigId()));
        lambdaQuery.andEq(IvyExecutor::getExecutorType, LambdaQuery.filterEmpty(vo.getExecutorType()));
        Options options = vo.getOptions();
        List<SortBy> sortBy = options.getSortBy();
        for (SortBy sort : sortBy) {
            if ("desc".equalsIgnoreCase(sort.getOrder())) {
                lambdaQuery.desc(com.ming.common.beetl.util.StrUtil.camelToSnake(sort.getKey()));
            } else {
                lambdaQuery.asc(StrUtil.camelToSnake(sort.getKey()));
            }
        }
        PageResult<IvyExecutor> page = lambdaQuery.page(options.getPage(), options.getItemsPerPage());
        return Result.OK(page);
    }

    @PostMapping("/add")
    @PermissionLimit(limit = false)
    public Result<?> add(@RequestBody IvyExecutor item){
        LambdaQuery<IvyExecutor> query = sqlManager.lambdaQuery(IvyExecutor.class);
        int i = query.insert(item);
        return Result.OK(i);
    }

    @PostMapping("/update")
    @PermissionLimit(limit = false)
    public Result<Object> update(@RequestBody IvyExecutor item){
        LambdaQuery<IvyExecutor> lambdaQuery = sqlManager.lambdaQuery(IvyExecutor.class);
        lambdaQuery.andNotEq(IvyExecutor::getId, item.getId());
        lambdaQuery.andEq(IvyExecutor::getExecutorId, item.getExecutorId());
        long count = lambdaQuery.count();
        if(count > 0){
            return Result.error("配置项ID重复");
        }
        int i = sqlManager.updateById(item);
        return Result.OK("更新成功", i);
    }

    @PostMapping("/delete")
    @PermissionLimit(limit = false)
    public Result<Integer> delete(@RequestBody IvyExecutor item){
        LambdaQuery<IvyExecutor> lambdaQuery = sqlManager.lambdaQuery(IvyExecutor.class);
        lambdaQuery.andEq(IvyExecutor::getId, item.getId());
        int i = lambdaQuery.delete();
        return Result.OK("删除成功",i);
    }
}
