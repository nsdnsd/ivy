规则文件之间可以用,或者;隔开：
规则文件为任何的*.xml形式即可，在v2.9.0之后，不存在老的表达式。但是文档为了兼容习惯，后面的示例依然会采用*.el.xml的形式。
```properties

liteflow.rule-source=config/flow1.el.xml,config/flow2.el.xml,config/flow3.el.xml
```
你也可以使用Spring EL表达式进行模糊匹配，加载多个配置文件：
模糊匹配只限于Springboot/Spring体系中，非Spring环境，模糊匹配不生效
```properties
liteflow.rule-source=config/**/*.el.xml
```

## XML在三种模式下的配置示例

在xml形式下的rule-source的配置方式如下：

本地文件：liteflow.rule-source=config/flow.el.xml

自定义配置源：liteflow.rule-source=el_xml:com.yomahub.liteflow.test.TestCustomParser



## JSON在三种模式下的配置示例

在json形式下的rule-source的配置方式如下：

本地文件：liteflow.rule-source=config/flow.el.json

自定义配置源：liteflow.rule-source=el_json:com.yomahub.liteflow.test.TestCustomParser

## YAML在三种模式下的配置示例

在yaml形式下的rule-source的配置方式如下：

本地文件：liteflow.rule-source=config/flow.el.yml

自定义配置源：liteflow.rule-source=el_yml:com.yomahub.liteflow.test.TestCustomParser