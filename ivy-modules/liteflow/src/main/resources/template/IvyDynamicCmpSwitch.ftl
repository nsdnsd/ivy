package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;

@LiteflowComponent("IvyDynamicCmpSwitch")
public class IvyDynamicCmpSwitch extends NodeSwitchComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpSwitch.class);

    @Override
    public String processSwitch() throws Exception {
        LOG.info("IvyDynamicCmpSwitch executed!");
        System.out.println("IvyDynamicCmpSwitch executed!");
        return "a";
    }
}