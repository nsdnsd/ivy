package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("IvyDynamicCmpCommon")
public class IvyDynamicCmpCommon extends NodeComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpCommon.class);

	@Override
	public void process() {
		LOG.info("IvyDynamicCmpCommon executed!");
		System.out.println("IvyDynamicCmpCommon executed!");
	}

}
