package com.ming.job.xxljob.entity;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "xxl_job_logglue")
public class XxlJobLogGlue {
	
	private Integer id;
	private Integer jobId;				// 任务主键ID
	private String glueType;		// GLUE类型	#com.xxl.job.core.glue.GlueTypeEnum
	private String glueSource;
	private String glueRemark;
	private Date addTime;
	private Date updateTime;


}
