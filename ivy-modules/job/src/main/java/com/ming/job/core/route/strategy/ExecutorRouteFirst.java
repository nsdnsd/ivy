package com.ming.job.core.route.strategy;

import com.ming.common.xxljob.biz.model.ReturnT;
import com.ming.common.xxljob.biz.model.TriggerParam;
import com.ming.job.core.route.ExecutorRouter;

import java.util.List;

/**
 * Created by xuxueli on 17/3/10.
 */
public class ExecutorRouteFirst extends ExecutorRouter {

    @Override
    public ReturnT<String> route(TriggerParam triggerParam, List<String> addressList){
        return new ReturnT<String>(addressList.get(0));
    }

}
