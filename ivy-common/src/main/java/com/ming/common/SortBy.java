package com.ming.common;

import lombok.Data;

@Data
public class SortBy {

    private String key;

    private String order;

}
