package com.ming.common.util;

import com.ming.common.util.coll.CollUtil;
import com.ming.common.util.number.BigNumberUtil;

public class CommonUtil {

    public static final BigNumberUtil bigNumberUtil = BigNumberUtil.NEW();
    public static final CollUtil collUtil = CollUtil.NEW();

    public static void main(String[] args) {
        String num = CommonUtil.bigNumberUtil.add("1.12e12346","2.31e12345");
        System.out.println(num);
    }
}
