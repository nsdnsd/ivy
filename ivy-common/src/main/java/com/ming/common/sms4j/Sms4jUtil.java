package com.ming.common.sms4j;


import cn.hutool.extra.spring.SpringUtil;
import com.ming.common.liteflow.core.node.SpringBeanUtil;
import org.dromara.email.api.MailClient;
import org.dromara.email.comm.entity.MailMessage;
import org.dromara.sms4j.core.factory.SmsFactory;

//使用文档：https://sms4j.com/doc3/
public class Sms4jUtil {

    public static void sendMsg(){
        Sms4jConfig sms4jConfig = SpringBeanUtil.getBean("sms4jConfig");
        // 创建SmsBlend 短信实例
        SmsFactory.createSmsBlend(sms4jConfig,"在配置中定义的configId");
    }

    public static void sendMail(MailMessage message){
        MailClient mailClient = SpringUtil.getBean("qqMailClient");
        if(mailClient != null){
            mailClient.send(message);
        }
    }

}
