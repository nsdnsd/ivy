package com.ming.common.beetl.vo;

import com.ming.common.Options;
import com.ming.common.beetl.entity.IvyDbDatasource;
import lombok.Data;

@Data
public class IvyDbDatasourceVo extends IvyDbDatasource {

    private Options options;

}
