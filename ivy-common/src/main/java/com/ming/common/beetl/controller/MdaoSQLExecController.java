package com.ming.common.beetl.controller;

import com.ming.common.beetl.entity.BaseEntity;
import com.ming.common.beetl.service.MdaoSQLExecService;
import com.ming.common.beetl.util.Result;
import com.ming.common.beetl.util.SQLExecUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/exec")
public class MdaoSQLExecController {

    @Resource
    private MdaoSQLExecService mdaoSQLExecService;

    @GetMapping("/selectAll")
    public Result selectAll(){
        List<? extends BaseEntity> list = mdaoSQLExecService.selectAll();
        BaseEntity baseEntity = list.stream().findFirst().get();
        SQLExecUtil.NEW().exec(baseEntity);
        return Result.OK(list);
    }

    @PostMapping("/insert")
    public Result insert(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLExecService.insert(data));
    }

    @PostMapping("/updateById")
    public Result updateById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLExecService.updateById(data));
    }

    @PostMapping("/deleteById")
    public Result deleteById(@RequestBody Map<String,Object> data) {
        return Result.OK(mdaoSQLExecService.deleteById(data));
    }


}
