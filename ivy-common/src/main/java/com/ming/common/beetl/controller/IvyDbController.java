package com.ming.common.beetl.controller;

import cn.hutool.core.map.MapUtil;
import com.ming.common.Options;
import com.ming.common.SortBy;
import com.ming.common.beetl.cache.CacheSqlManagerUtil;
import com.ming.common.beetl.entity.BaseEntity;
import com.ming.common.beetl.entity.IvyDbDatasource;
import com.ming.common.beetl.entity.IvyDbSqlManager;
import com.ming.common.beetl.entity.SQLManagerEntity;
import com.ming.common.beetl.enums.DictEnums;
import com.ming.common.beetl.util.*;
import com.ming.common.beetl.vo.IvyDbSqlManagerVo;
import com.ming.common.beetl.vo.IvyDbTableVo;
import com.ming.common.beetl.vo.IvyPageResult;
import com.ming.common.init.LogInfo;
import com.ming.common.init.LogInit;
import com.ming.common.util.ClassFieldUtil;
import com.ming.common.util.CommonUtil;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.zaxxer.hikari.HikariDataSource;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.SQLReady;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.core.query.LambdaQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/db")
public class IvyDbController {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDbController.class);

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/tables")
    @PermissionLimit(limit = false)
    public Result<?> tables(@RequestBody Map<String,Object> map) {
        Long id = Long.parseLong(String.valueOf(map.get("id")));
        SQLManager manager = CacheSqlManagerUtil.getCache(String.valueOf(id), SQLManager.class);
        Set<String> tableSet = manager.getMetaDataManager().allTable();
        List<Map<String, Object>> tableList = tableSet.stream().map(m -> MapUtil.builder(new HashMap<String, Object>())
                .put("title", m)
                .put("value", m)
                .put("prependIcon", "tabler-table")
                .build()).collect(Collectors.toList());
        return Result.OK(tableList);
    }

    @PostMapping("/table/data")
    @PermissionLimit(limit = false)
    public Result<?> tableData(@RequestBody IvyDbTableVo vo) {
        SQLManager manager = CacheSqlManagerUtil.getCache(String.valueOf(vo.getId()), SQLManager.class);
        //PageResult<? extends BaseEntity> pageResult = SQLManagerUtil.NEW().sqlManager(manager).tableName(vo.getTableName()).selectPage(vo.getOptions());
        IvyPageResult ivyPageResult = SQLManagerUtil.NEW().sqlManager(manager).tableName(vo.getTableName()).selectIvyPage(vo.getOptions());
        return Result.OK(ivyPageResult);
    }
}
