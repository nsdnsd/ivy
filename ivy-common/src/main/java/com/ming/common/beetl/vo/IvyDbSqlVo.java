package com.ming.common.beetl.vo;

import com.ming.common.Options;
import com.ming.common.beetl.entity.IvyDbSql;
import com.ming.common.beetl.entity.IvyDbSqlItem;
import lombok.Data;

import java.util.List;

@Data
public class IvyDbSqlVo extends IvyDbSql {

    private Options options;

    private List<IvyDbSqlItem> columnList;
    private List<IvyDbSqlItem> whereList;
    private List<IvyDbSqlItem> groupByList;
    private List<IvyDbSqlItem> orderByList;
    private List<IvyDbSqlItem> limitList;

    private Boolean formatSql;

}
