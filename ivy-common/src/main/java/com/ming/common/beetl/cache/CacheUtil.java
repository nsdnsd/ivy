package com.ming.common.beetl.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class CacheUtil {

    private static final Cache<String,Object> caffeineM5 = Caffeine.newBuilder().initialCapacity(10).expireAfterWrite(5, TimeUnit.MINUTES).build();
    private static final Cache<String,Object> caffeineY1 = Caffeine.newBuilder().initialCapacity(10).expireAfterWrite(365, TimeUnit.DAYS).build();
    private Cache<String,Object> caffeineCache;

    private  <T> T getCache(String key,Class<T> t){
        caffeineCache.getIfPresent(key);
        return (T) caffeineCache.asMap().get(key);
    }

    private void putAndUpdateCache(String key,Object value){
        caffeineCache.put(key,value);
    }

    private void removeCache(String key){
        caffeineCache.asMap().remove(key);
    }

    private void removesCache(String... keys){
        for (String key : keys){
            caffeineCache.asMap().remove(key);
        }
    }

    private String key;
    private Class<?> classType;
    private Object result;

    public static CacheUtil NEW(){
        return new CacheUtil();
    }

    public CacheUtil caffeine(Cache<String,Object> caffeineCache){ this.caffeineCache = caffeineCache; return this; }
    public CacheUtil caffeineM5(){ this.caffeineCache = caffeineM5; return this; }
    public CacheUtil caffeineY1(){ this.caffeineCache = caffeineY1; return this; }

    public CacheUtil key(Object... keys){
        this.key = getKey(keys);
        return this;
    }

    public CacheUtil classType(Class<?> classType){
        this.classType = classType;
        return this;
    }

    public CacheUtil put(ResultConsumer<CacheUtil> action){
        Object o = getCache(key, classType);
        if(o == null){
            this.result = action.accept(this);
            putAndUpdateCache(key,this.result);
        }else{
            this.result = o;
        }
        return this;
    }

    public <T> T get(){
        return (T) result;
    }

    public <T> T get(T defaultVal){
        if(result == null){
            return defaultVal;
        }
        return (T) result;
    }

    public static String getKey(Object... args){
        StringBuilder sb = new StringBuilder();
        for (Object arg : args){
            sb.append(arg.toString()).append("_");
        }
        return sb.append("KEY").toString();
    }

    public int clear() {
        ConcurrentMap<String, Object> map = caffeineCache.asMap();
        int size1 = map.size();
        map.clear();
        int size2 = map.size();
        return size1 - size2;
    }

/*    public static void main(String[] args) {
        //List<Map<String,Object>> resultList = CacheUtil.NEW().caffeine(cache).key("tzxx",vo).put(m -> homeGxcService.tzxx(vo)).get();
    }*/
}
