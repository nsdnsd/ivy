package com.ming.common.beetl.entity;

import com.ming.common.anno.Describe;
import com.ming.common.anno.DescribeItem;
import lombok.Data;

@Data
public class IvyDbSqlOper {

    @Describe(value = "column",items = {
            @DescribeItem(value = "column", desc = "column"),
            @DescribeItem(value = "distinct", desc = "distinct"),
            @DescribeItem(value = "count", desc = "count"),
            @DescribeItem(value = "max", desc = "max"),
            @DescribeItem(value = "min", desc = "min"),
    })
    private String column;

    @Describe(value = "where",items = {
            @DescribeItem(value = "andEq", desc = "andEq"),
            @DescribeItem(value = "andNotEq", desc = "andNotEq"),
            @DescribeItem(value = "andLike", desc = "andLike"),
            @DescribeItem(value = "andNotLike", desc = "andNotLike"),
            @DescribeItem(value = "andIn", desc = "andIn"),
            @DescribeItem(value = "andNotIn", desc = "andNotIn"),
            @DescribeItem(value = "andBetween", desc = "andBetween"),
            @DescribeItem(value = "andNotBetween", desc = "andNotBetween"),
            @DescribeItem(value = "andIsNull", desc = "andIsNull"),
            @DescribeItem(value = "andIsNotNull", desc = "andIsNotNull"),
            @DescribeItem(value = "andGreat", desc = "andGreat"),
            @DescribeItem(value = "andGreatEq", desc = "andGreatEq"),
            @DescribeItem(value = "andLess", desc = "andLess"),
            @DescribeItem(value = "andLessEq", desc = "andLessEq"),

            @DescribeItem(value = "orEq", desc = "orEq"),
            @DescribeItem(value = "orNotEq", desc = "orNotEq"),
            @DescribeItem(value = "orLike", desc = "orLike"),
            @DescribeItem(value = "orNotLike", desc = "orNotLike"),
            @DescribeItem(value = "orIn", desc = "orIn"),
            @DescribeItem(value = "orNotIn", desc = "orNotIn"),
            @DescribeItem(value = "orBetween", desc = "orBetween"),
            @DescribeItem(value = "orNotBetween", desc = "orNotBetween"),
            @DescribeItem(value = "orIsNull", desc = "orIsNull"),
            @DescribeItem(value = "orIsNotNull", desc = "orIsNotNull"),
            @DescribeItem(value = "orGreat", desc = "orGreat"),
            @DescribeItem(value = "orGreatEq", desc = "orGreatEq"),
            @DescribeItem(value = "orLess", desc = "orLess"),
            @DescribeItem(value = "orLessEq", desc = "orLessEq"),
    })
    private String where;

    @Describe(value = "orderBy",items = {
            @DescribeItem(value = "asc", desc = "asc"),
            @DescribeItem(value = "desc", desc = "desc"),
//            @DescribeItem(value = "orderBy", desc = "orderBy"),
    })
    private String orderBy;

    @Describe(value = "limit",items = {
            @DescribeItem(value = "limit", desc = "limit"),
    })
    private String limit;

    @Describe(value = "groupBy",items = {
            @DescribeItem(value = "groupBy", desc = "groupBy"),
    })
    private String groupBy;
}
