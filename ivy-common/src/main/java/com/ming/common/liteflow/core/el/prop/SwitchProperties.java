package com.ming.common.liteflow.core.el.prop;

import lombok.Data;

@Data
public class SwitchProperties extends Properties {

    private Object[] to;
    private Object defaultOpt;

}
