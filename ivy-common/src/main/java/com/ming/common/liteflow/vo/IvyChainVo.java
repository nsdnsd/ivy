package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.chain.IvyChain;
import lombok.Data;

@Data
public class IvyChainVo extends IvyChain {

    private Options options;

}
