package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.dynamic.IvyDynamicClass;
import lombok.Data;

@Data
public class IvyDynamicClassVo extends IvyDynamicClass {

    private Options options;

    private String methodName;

    private Object[] params;

}
