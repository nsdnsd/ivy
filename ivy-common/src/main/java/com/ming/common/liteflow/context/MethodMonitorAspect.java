//package com.ming.common.liteflow.context;
//
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class MethodMonitorAspect {
//
//    // 定义一个切入点，匹配所有方法
//    @Pointcut("execution(* com.ivy.cla.*.*(..))")
//    public void allMethods() {
//    }
//
//    // 定义一个环绕通知，在方法调用前后执行
//    @Around("allMethods()")
//    public Object interceptAllMethods(ProceedingJoinPoint joinPoint) throws Throwable {
//        // 在方法调用前插入监控代码
//        System.out.println("Method " + joinPoint.getSignature().getName() + " is about to be called");
//
//        try {
//            // 调用原始方法
//            Object result = joinPoint.proceed();
//
//            // 在方法调用后插入监控代码
//            System.out.println("Method " + joinPoint.getSignature().getName() + " has been called");
//
//            return result;
//        } catch (Exception e) {
//            // 在方法调用抛出异常时插入监控代码
//            System.out.println("Method " + joinPoint.getSignature().getName() + " has thrown an exception: " + e.getMessage());
//            throw e;
//        }
//    }
//}
