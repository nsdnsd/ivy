package com.ming.common.liteflow.core.el.bus;

import cn.hutool.core.util.StrUtil;
import com.ming.common.liteflow.core.node.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.WhileELWrapper;

public class ELBusWhile {

    private ELWrapper wrapper;

    public static ELBusWhile NEW(){
        return new ELBusWhile();
    }

    public ELBusWhile node(IvyCmp info){
        WhileELWrapper whileELWrapper = ELBus.whileOpt(ELBusNode.NEW().node(info).toELWrapper());
        if(StrUtil.isNotBlank(info.getCmpId())){
            whileELWrapper.id(info.getCmpId());
        }
        if(StrUtil.isNotBlank(info.getCmpTag())){
            whileELWrapper.tag(info.getCmpTag());
        }
        if(info.getCmpParallel() != null){
            whileELWrapper.parallel(info.getCmpParallel());
        }
        if(StrUtil.isNotBlank(info.getCmpDoOpt())){
            whileELWrapper.doOpt(info.getCmpDoOpt());
        }
        if(StrUtil.isNotBlank(info.getCmpBreakOpt())){
            whileELWrapper.breakOpt(info.getCmpBreakOpt());
        }
        if(info.getCmpMaxWaitSeconds() != null){
            whileELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
        }
        this.wrapper = whileELWrapper;
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
