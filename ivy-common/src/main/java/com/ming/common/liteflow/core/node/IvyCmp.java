package com.ming.common.liteflow.core.node;

import com.ming.common.generate.template.annotation.Generate;
import com.ming.common.generate.template.annotation.database.Column;
import com.ming.common.generate.template.annotation.database.PrimaryKey;
import com.ming.common.generate.template.annotation.database.Text;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_cmp")
@Generate(isEffective = true, moduleName = "db", tableName = "ivy_cmp", desc = "组件信息")
public class IvyCmp {

    @PrimaryKey
    @Column
    private Long id;

    @Column(len = 64)
    private String componentId;

    @Column(len = 64)
    private String componentName;

    @Column
    private String type;//

    private NodeTypeEnum nodeType;//switch,for

    @Column
    @Text
    private String script;//

    @Column
    private String language;//

    @Column(len = 256)
    private String clazz;//

    @Column
    private Long fallbackId;//

    private String fallbackType;//

    @Column(len = 256)
    private String el;

    @Column(len = 256)
    private String elFormat;

    @Column(len = 256)
    private String cmpPre;

    @Column(len = 256)
    private String cmpFinallyOpt;

    @Column
    private String cmpId;

    @Column
    private String cmpTag;

    @Column
    private Integer cmpMaxWaitSeconds;

    @Column(len = 256)
    private String cmpTo;

    @Column(len = 256)
    private String cmpDefaultOpt;

    private Object cmpToEL;
    private Object cmpDefaultOptEL;

    @Column(len = 256)
    private String cmpTrueOpt;
    private Object cmpTrueOptEL;

    @Column(len = 256)
    private String cmpFalseOpt;
    private Object cmpFalseOptEL;

    @Column
    private Boolean cmpParallel;

    @Column(len = 256)
    private String cmpDoOpt;

    @Column(len = 256)
    private String cmpBreakOpt;

    @Column(len = 32)
    private String cmpDataName;

    @Text
    @Column
    private String cmpData;
}
