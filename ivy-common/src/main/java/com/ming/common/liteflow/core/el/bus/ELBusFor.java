package com.ming.common.liteflow.core.el.bus;

import cn.hutool.core.util.StrUtil;
import com.ming.common.liteflow.core.node.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.ForELWrapper;

public class ELBusFor {

    private ELWrapper wrapper;

    public static ELBusFor NEW(){
        return new ELBusFor();
    }

    public ELBusFor node(IvyCmp info){
        ForELWrapper forELWrapper = ELBus.forOpt(ELBusNode.NEW().node(info).toELWrapper());
        if(StrUtil.isNotBlank(info.getCmpId())){
            forELWrapper.id(info.getCmpId());
        }
        if(StrUtil.isNotBlank(info.getCmpTag())){
            forELWrapper.tag(info.getCmpTag());
        }
        if(info.getCmpParallel() != null){
            forELWrapper.parallel(info.getCmpParallel());
        }
        if(StrUtil.isNotBlank(info.getCmpDoOpt())){
            forELWrapper.doOpt(info.getCmpDoOpt());
        }
        if(StrUtil.isNotBlank(info.getCmpBreakOpt())){
            forELWrapper.breakOpt(info.getCmpBreakOpt());
        }
        if(info.getCmpMaxWaitSeconds() != null){
            forELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
        }
        this.wrapper = forELWrapper;
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
