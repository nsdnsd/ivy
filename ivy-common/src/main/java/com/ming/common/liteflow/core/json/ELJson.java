package com.ming.common.liteflow.core.json;

public class ELJson {

    public static final String when_json_1 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\"id\": \"a\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"a\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"b\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"b\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"c\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"c\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"d\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"d\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"e\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"e\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"}\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "  ]\n" +
            "}";

    public static final String then_json_1 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\"id\": \"a\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"a\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"b\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"b\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"c\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"c\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"d\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"d\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"e\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"e\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"}\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "    {\"sourceNodeId\": \"a\", \"targetNodeId\": \"b\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"b\", \"targetNodeId\": \"c\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"c\", \"targetNodeId\": \"d\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"d\", \"targetNodeId\": \"e\", \"properties\": {}, \"text\": \"\"},\n" +
            "  ]\n" +
            "}";

    public static final String bx_json_1 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\"id\": \"a0\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"a0\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"a\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"a\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"b\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"b\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"c\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"c\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"d\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"d\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"f\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"f\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"},\n" +
            "    {\"id\": \"e\", \"type\": \"NodeComponent\", \"properties\": {\"componentId\": \"e\", \"ui\": \"node-red\"}, \"text\": \"基础组件\"}\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "    {\"sourceNodeId\": \"a0\", \"targetNodeId\": \"a\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"a\", \"targetNodeId\": \"b\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"b\", \"targetNodeId\": \"c\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"c\", \"targetNodeId\": \"d\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"a\", \"targetNodeId\": \"e\", \"properties\": {}, \"text\": \"\"},\n" +
            "    {\"sourceNodeId\": \"e\", \"targetNodeId\": \"d\", \"properties\": {}, \"text\": \"\"}\n" +
            "    {\"sourceNodeId\": \"d\", \"targetNodeId\": \"f\", \"properties\": {}, \"text\": \"\"}\n" +
            "  ]\n" +
            "}";

    public static final String bx_json_2 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\n" +
            "      \"id\": \"1af21635-b6b2-4077-a97e-c2fd13944e74\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"a\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"b\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"c\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"d\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"e\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"f\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"1af21635-b6b2-4077-a97e-c2fd13944e74\",\n" +
            "      \"targetNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String bx_json_3 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\n" +
            "      \"id\": \"1af21635-b6b2-4077-a97e-c2fd13944e74\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"a\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"a\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"d\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"d\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"f\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"f\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"x\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"e\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"e\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"h\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"h\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"7bdbdd4d-e91c-433c-8407-2d5c7108f0a3\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"y\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"基础组件\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"58fcdbb5-1c0b-44cc-8bce-7fbd5b27eb70\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"b\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"b\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"776dad94-5f03-4701-9c67-3987f7d95e25\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"c\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"c\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"24563a59-64d2-4516-b7d8-d705c3d3b874\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"i\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"i\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"e67b01bf-111a-4b48-b48e-4bb300a4bce5\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"g\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"g\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"d3a68a7f-16a7-4e34-bdc2-0ab7ef2cf196\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"j\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"j\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"994580b0-e0bf-4ef8-adfd-27d8ab1c7c14\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"k\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"k\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"e5ddb864-0c1d-4a61-9c5b-d1513972b87d\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"l\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"l\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"e02f5f37-02c2-4cf1-a87f-7549f4a0e7eb\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"o\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"o\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"9c9904bb-7d32-4725-9758-f40a8c70a6c9\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"m\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"m\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"228974f6-0bec-4893-86c5-c16716981309\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"n\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"n\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"f0c4b368-63a6-43a1-b0a2-76a4fc46ed74\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"targetNodeId\": \"7bdbdd4d-e91c-433c-8407-2d5c7108f0a3\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"1af21635-b6b2-4077-a97e-c2fd13944e74\",\n" +
            "      \"targetNodeId\": \"58fcdbb5-1c0b-44cc-8bce-7fbd5b27eb70\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"58fcdbb5-1c0b-44cc-8bce-7fbd5b27eb70\",\n" +
            "      \"targetNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"1af21635-b6b2-4077-a97e-c2fd13944e74\",\n" +
            "      \"targetNodeId\": \"776dad94-5f03-4701-9c67-3987f7d95e25\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"776dad94-5f03-4701-9c67-3987f7d95e25\",\n" +
            "      \"targetNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"targetNodeId\": \"24563a59-64d2-4516-b7d8-d705c3d3b874\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"24563a59-64d2-4516-b7d8-d705c3d3b874\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"b19e09ec-9286-4073-9144-5c64b6bbcfc1\",\n" +
            "      \"targetNodeId\": \"e67b01bf-111a-4b48-b48e-4bb300a4bce5\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"e67b01bf-111a-4b48-b48e-4bb300a4bce5\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4b2ca9f6-0d3d-4e6c-a1fb-56c4d03409fc\",\n" +
            "      \"targetNodeId\": \"d3a68a7f-16a7-4e34-bdc2-0ab7ef2cf196\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"d3a68a7f-16a7-4e34-bdc2-0ab7ef2cf196\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"4c884a40-eeec-4a23-9246-53dfc3ef6a92\",\n" +
            "      \"targetNodeId\": \"994580b0-e0bf-4ef8-adfd-27d8ab1c7c14\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"994580b0-e0bf-4ef8-adfd-27d8ab1c7c14\",\n" +
            "      \"targetNodeId\": \"e5ddb864-0c1d-4a61-9c5b-d1513972b87d\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"e5ddb864-0c1d-4a61-9c5b-d1513972b87d\",\n" +
            "      \"targetNodeId\": \"e02f5f37-02c2-4cf1-a87f-7549f4a0e7eb\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"e02f5f37-02c2-4cf1-a87f-7549f4a0e7eb\",\n" +
            "      \"targetNodeId\": \"0492a373-a6e8-4f8a-8347-d9ec3987f15a\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"994580b0-e0bf-4ef8-adfd-27d8ab1c7c14\",\n" +
            "      \"targetNodeId\": \"9c9904bb-7d32-4725-9758-f40a8c70a6c9\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"9c9904bb-7d32-4725-9758-f40a8c70a6c9\",\n" +
            "      \"targetNodeId\": \"228974f6-0bec-4893-86c5-c16716981309\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"228974f6-0bec-4893-86c5-c16716981309\",\n" +
            "      \"targetNodeId\": \"e02f5f37-02c2-4cf1-a87f-7549f4a0e7eb\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static final String bx_json_4 = "{\n" +
            "  \"nodes\": [\n" +
            "    {\n" +
            "      \"id\": \"9a44253b-048e-4982-9db4-2793c78998d6\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"a\",\n" +
            "        \"componentName\": \"a\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"a\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"577102b9-d8aa-47a5-855b-8f8f9a02427d\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"c\",\n" +
            "        \"componentName\": \"c\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"c\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"c00e0d17-bee0-46d1-aacc-ceccd3782994\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"b\",\n" +
            "        \"componentName\": \"b\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"b\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"eeff7c39-4bb7-4f4c-b6aa-722ce9a87f3d\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"d\",\n" +
            "        \"componentName\": \"d\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"d\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"f458634b-e08f-4446-a77d-d0919f2b9fce\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"f\",\n" +
            "        \"componentName\": \"f\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"f\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"57205f45-4130-4d6f-b8df-b317913deeee\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"e\",\n" +
            "        \"componentName\": \"e\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"e\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"279bbfb3-f2b1-4c1d-b60f-fbe3ba18c82b\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"x\",\n" +
            "        \"componentName\": \"x\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"x\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4cb989b6-755b-4d90-9eaf-95b3e41654b6\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"y\",\n" +
            "        \"componentName\": \"y\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"y\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"930a4104-f9a5-4838-8bc6-cf9ea60b21af\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"h\",\n" +
            "        \"componentName\": \"h\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"h\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"7bb8deef-0844-44aa-96eb-8e50f69fc137\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"i\",\n" +
            "        \"componentName\": \"i\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"i\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"d99220a7-99fb-497d-b896-0a373568f3f2\",\n" +
            "      \"type\": \"NodeComponent\",\n" +
            "      \"properties\": {\n" +
            "        \"componentId\": \"j\",\n" +
            "        \"componentName\": \"j\",\n" +
            "        \"ui\": \"node-red\"\n" +
            "      },\n" +
            "      \"text\": \"j\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"edges\": [\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"279bbfb3-f2b1-4c1d-b60f-fbe3ba18c82b\",\n" +
            "      \"targetNodeId\": \"4cb989b6-755b-4d90-9eaf-95b3e41654b6\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"9a44253b-048e-4982-9db4-2793c78998d6\",\n" +
            "      \"targetNodeId\": \"577102b9-d8aa-47a5-855b-8f8f9a02427d\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"9a44253b-048e-4982-9db4-2793c78998d6\",\n" +
            "      \"targetNodeId\": \"c00e0d17-bee0-46d1-aacc-ceccd3782994\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"c00e0d17-bee0-46d1-aacc-ceccd3782994\",\n" +
            "      \"targetNodeId\": \"eeff7c39-4bb7-4f4c-b6aa-722ce9a87f3d\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"577102b9-d8aa-47a5-855b-8f8f9a02427d\",\n" +
            "      \"targetNodeId\": \"eeff7c39-4bb7-4f4c-b6aa-722ce9a87f3d\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"eeff7c39-4bb7-4f4c-b6aa-722ce9a87f3d\",\n" +
            "      \"targetNodeId\": \"57205f45-4130-4d6f-b8df-b317913deeee\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"eeff7c39-4bb7-4f4c-b6aa-722ce9a87f3d\",\n" +
            "      \"targetNodeId\": \"f458634b-e08f-4446-a77d-d0919f2b9fce\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"57205f45-4130-4d6f-b8df-b317913deeee\",\n" +
            "      \"targetNodeId\": \"930a4104-f9a5-4838-8bc6-cf9ea60b21af\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"930a4104-f9a5-4838-8bc6-cf9ea60b21af\",\n" +
            "      \"targetNodeId\": \"279bbfb3-f2b1-4c1d-b60f-fbe3ba18c82b\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"f458634b-e08f-4446-a77d-d0919f2b9fce\",\n" +
            "      \"targetNodeId\": \"7bb8deef-0844-44aa-96eb-8e50f69fc137\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"f458634b-e08f-4446-a77d-d0919f2b9fce\",\n" +
            "      \"targetNodeId\": \"d99220a7-99fb-497d-b896-0a373568f3f2\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"7bb8deef-0844-44aa-96eb-8e50f69fc137\",\n" +
            "      \"targetNodeId\": \"279bbfb3-f2b1-4c1d-b60f-fbe3ba18c82b\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sourceNodeId\": \"d99220a7-99fb-497d-b896-0a373568f3f2\",\n" +
            "      \"targetNodeId\": \"279bbfb3-f2b1-4c1d-b60f-fbe3ba18c82b\",\n" +
            "      \"properties\": {},\n" +
            "      \"text\": \"\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
