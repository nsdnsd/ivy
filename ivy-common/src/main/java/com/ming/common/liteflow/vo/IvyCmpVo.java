package com.ming.common.liteflow.vo;

import com.ming.common.Options;
import com.ming.common.liteflow.core.node.IvyCmp;
import lombok.Data;

@Data
public class IvyCmpVo extends IvyCmp {

    private Options options;

}
