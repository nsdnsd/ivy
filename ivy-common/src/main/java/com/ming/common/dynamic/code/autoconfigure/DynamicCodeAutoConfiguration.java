/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code.autoconfigure;

import com.ming.common.dynamic.code.IClassExecuter;
import com.ming.common.dynamic.code.IStringCompiler;
import com.ming.common.dynamic.code.RunClassHandler;
import com.ming.common.dynamic.code.RunSourceHandler;
import com.ming.common.dynamic.code.config.RunClassProperties;
import com.ming.common.dynamic.code.config.RunSourceProperties;
import com.ming.common.dynamic.code.core.ClassExecuter;
import com.ming.common.dynamic.code.core.JaninoCompiler;
import com.ming.common.dynamic.code.core.StringJavaCompiler;
import com.ming.common.dynamic.code.dto.ExecuteResult;
import com.ming.common.dynamic.code.factory.AbstractCompilerFactory;
import com.ming.common.dynamic.code.factory.StandardCompilerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * 自动配置
 *
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月18日
 */
@Configuration
@EnableConfigurationProperties(DynamicCodeProperties.class)
public class DynamicCodeAutoConfiguration {
    private final DynamicCodeProperties properties;

    public DynamicCodeAutoConfiguration(DynamicCodeProperties properties) {
        this.properties = properties;
    }

    @Bean
    @ConditionalOnMissingBean
    public IStringCompiler stringCompiler() throws Exception {
        if (properties.getJdkToolUrl() != null && !properties.getJdkToolUrl().isEmpty()) {
            URL jdkToolUrl = new URL(properties.getJdkToolUrl());
            AbstractCompilerFactory compilerFactory = new StandardCompilerFactory(jdkToolUrl);
            return new StringJavaCompiler(compilerFactory);
        } else {
            return new JaninoCompiler();
        }
    }

    @Bean
    @ConditionalOnMissingBean
    public IClassExecuter<ExecuteResult> classExecuter() {
        return new ClassExecuter();
    }

    @Bean
    @ConditionalOnMissingBean
    public RunClassHandler runClassHandler(IStringCompiler compiler,
                                           IClassExecuter<ExecuteResult> executer) {
        RunClassProperties classProperties = properties.getClassHandler();
        return new RunClassHandler(compiler, executer, classProperties, hacker());
    }

    @Bean
    @ConditionalOnMissingBean
    public RunSourceHandler runSourceHandler(IStringCompiler compiler,
                                             IClassExecuter<ExecuteResult> executer) {
        RunSourceProperties sourceProperties = properties.getSourceHandler();
        return new RunSourceHandler(compiler, executer, sourceProperties, hacker(),
                sourceProperties.getCacheSize());
    }

    public Map<String, String> hacker() {
        if (properties.getHacker() == null) {
            Map<String, String> hackers = new HashMap<>(1);
            hackers.put("java/io/File", "com/zhg2yqq/wheels/dynamic/code/hack/HackFile");
            return hackers;
        } else {
            return properties.getHacker();
        }
    }
}
