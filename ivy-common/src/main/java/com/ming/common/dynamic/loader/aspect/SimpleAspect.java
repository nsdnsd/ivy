package com.ming.common.dynamic.loader.aspect;


import org.springframework.util.StopWatch;
import java.lang.reflect.Method;

/**
 * 切面类，简单的输出和记录方法执行时间
 */
public class SimpleAspect implements IAspect {

    private StopWatch sw;

    @Override
    public void before(Object target, Method method, Object[] args) {
        sw = new StopWatch(target.getClass().getName() + " " + method.getName());
        System.out.println("SimpleAspect before " + sw.getId());
        sw.start();
    }

    @Override
    public void after(Object target, Method method, Object[] args, Object result) {
        sw.stop();
        System.out.println("SimpleAspect after " + sw.getId());
        System.out.println(sw.shortSummary());
    }

    @Override
    public void afterThrow(Object target, Method method, Object[] args, Throwable cause) {
        sw.stop();
        System.out.println("SimpleAspect afterThrow " + sw.getId() + " " + cause.getMessage());
        System.out.println(sw.shortSummary());
    }
}
