package com.ming.common.dynamic.loader.bean_loader;


import com.ming.common.dynamic.loader.SpringContextUtil;
import com.ming.common.dynamic.loader.class_loader.DynamicClass;

public class DynamicBean {

    private DynamicClass dynamicClass;

    public DynamicBean(DynamicClass dynamicClass) {
        this.dynamicClass = dynamicClass;
    }

    public static DynamicBean init(DynamicClass dynamicClass) {
        System.out.println("初始化bean fullClassName:{}" + dynamicClass.getFullClassName());
        return new DynamicBean(dynamicClass);
    }

    public String load() {
        String beanName = SpringContextUtil.beanName(dynamicClass.getFullClassName());
        //销毁Bean
        SpringContextUtil.destroy(beanName);
        //每次都是new新的ClassLoader对象
        Class<?> type = dynamicClass.compiler().load();
        SpringContextUtil.registerSingleton(type);
        return beanName;
    }
}

