/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.googlecode.concurrentlinkedhashmap.Weighers;
import com.ming.common.dynamic.code.config.RunSourceProperties;
import com.ming.common.dynamic.code.dto.*;
import com.ming.common.dynamic.code.exception.BaseDynamicException;
import com.ming.common.dynamic.code.exception.ClassLoadException;
import com.ming.common.dynamic.code.exception.CompileException;
import com.ming.common.dynamic.code.util.ClassUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * 执行Java代码，缓存编译后的Class（容器大小默认100，采用最近最少使用策略）
 * 
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月8日
 */
public class RunSourceHandler extends AbstractRunHandler<ExecuteResult, ClassBean> {
    private static final int DEFAULT_CACHE_SIZE = 100;
    /**
     * 缓存类
     */
    private final ConcurrentMap<String, ClassBean> cacheClasses;

    /**
     * 源码处理程序
     * 
     * @param compiler 编译器
     * @param executer 执行器
     * @param properties 配置
     */
    public RunSourceHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, RunSourceProperties properties) {
        this(compiler, executer, properties, null);
    }

    /**
     * 源码处理程序
     * 
     * @param compiler 编译器
     * @param executer 执行器
     * @param properties 配置
     * @param hackers
     *            安全替换（key:待替换的类名,例如:java/lang/System，value:替换成的类名,例如:com/zhg2yqq/wheels/dynamic/code/hack/HackSystem）
     */
    public RunSourceHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, RunSourceProperties properties,
            Map<String, String> hackers) {
        this(compiler, executer, properties, hackers, DEFAULT_CACHE_SIZE);
    }

    /**
     * 源码处理程序
     * 
     * @param compiler 编译器
     * @param executer 执行器
     * @param properties 配置
     * @param cacheSize 缓存Class容器大小，超出容器大小将会以最近最少使用原则删除原数据
     */
    public RunSourceHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, RunSourceProperties properties,
            int cacheSize) {
        this(compiler, executer, properties, null, cacheSize);
    }

    /**
     * 源码处理程序
     * 
     * @param compiler 编译器
     * @param executer 执行器
     * @param properties 配置
     * @param hackers
     *            安全替换（key:待替换的类名,例如:java/lang/System，value:替换成的类名,例如:com/zhg2yqq/wheels/dynamic/code/hack/HackSystem）
     * @param cacheSize 缓存Class容器大小，超出容器大小将会以最近最少使用原则删除原数据
     */
    public RunSourceHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, RunSourceProperties properties,
            Map<String, String> hackers, int cacheSize) {
        super(compiler, executer, properties, hackers);
        this.cacheClasses = new ConcurrentLinkedHashMap.Builder<String, ClassBean>()
                .maximumWeightedCapacity(cacheSize).weigher(Weighers.singleton()).build();
    }

    /**
     * 执行Java方法
     * 
     * @param source 源码
     * @param methodName 方法名，例如getTime
     * @param parameters 方法参数
     * @param singleton 是否单例执行
     * @return 方法执行结果
     * @throws BaseDynamicException .
     */
    @Override
    public ExecuteResult runMethod(String source, String methodName, Parameters parameters,
                                   boolean singleton) throws BaseDynamicException {
        return this.runMethod(source, methodName, parameters, singleton, false);
    }

    /**
     * 执行Java方法
     * 
     * @param source 源码
     * @param methodName 方法名，例如getTime
     * @param parameters 方法参数
     * @param singleton 是否单例执行
     * @param reloadClass 是否重新编译加载类；当为true时，singleton单例失效（即重新编译后，都须重新创建实例）
     * @return 方法执行结果
     * @throws BaseDynamicException .
     */
    public ExecuteResult runMethod(String source, String methodName, Parameters parameters,
                                   boolean singleton, boolean reloadClass) throws BaseDynamicException  {
        ClassBean classBean = null;
        if (reloadClass) {
            classBean = this.loadClassFromSource(source);
        } else {
            classBean = this.loadOrginalClassFromSource(source);
        }
        ExecuteParameter<ClassBean> parameter = new ExecuteParameter<>(classBean, methodName,
                parameters);
        ExecuteCondition condition = new ExecuteCondition(singleton,
                getProperties().isCalExecuteTime(), getProperties().getExecuteTimeOut());
        return getExecuter().runMethod(parameter, condition);
    }

    /**
     * 加载Class，如果重复加载Class类，将直接返回之前的Class。
     * 每个class都对应独立的ClassLoader
     * 
     * @param sourceStr 源码
     * @return 类
     * @throws CompileException .
     * @throws ClassLoadException .
     */
    public ClassBean loadOrginalClassFromSource(String sourceStr)
        throws CompileException, ClassLoadException {
        String fullClassName = ClassUtils.getFullClassName(sourceStr);
        try {
            return this.getClassCache().computeIfAbsent(fullClassName, className -> {
                try {
                    Class<?> clazz = loadClass(className, sourceStr);
                    return new ClassBean(clazz);
                } catch (CompileException | ClassLoadException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (RuntimeException e) {
            if (e.getCause() instanceof CompileException) {
                throw (CompileException) e.getCause();
            } else if (e.getCause() instanceof ClassLoadException) {
                throw (ClassLoadException) e.getCause();
            } else {
                throw e;
            }
        }
    }

    @Override
    protected Map<String, ClassBean> getClassCache() {
        return cacheClasses;
    }

    @Override
    protected ClassBean buildClassBean(Class<?> clazz) {
        return new ClassBean(clazz);
    }
}