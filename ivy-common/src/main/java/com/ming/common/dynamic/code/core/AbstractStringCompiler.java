/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code.core;

import com.ming.common.dynamic.code.IStringCompiler;
import com.ming.common.dynamic.code.factory.AbstractCompilerFactory;
import com.ming.common.dynamic.code.factory.StandardCompilerFactory;

import javax.tools.JavaCompiler;


/**
 * Java源码文本编译
 * 
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月26日
 */
public abstract class AbstractStringCompiler implements IStringCompiler {
    // java编译器
    private JavaCompiler compiler;

    public AbstractStringCompiler() {
        this(new StandardCompilerFactory());
    }

    public AbstractStringCompiler(AbstractCompilerFactory factory) {
        this.compiler = factory.getCompiler();
    }

    public JavaCompiler getCompiler() {
        return compiler;
    }
}
