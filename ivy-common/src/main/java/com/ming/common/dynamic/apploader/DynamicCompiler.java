//package com.ming.common.dynamic.apploader;
//
//import javax.tools.*;
//import java.io.*;
//import java.net.URL;
//import java.net.URLClassLoader;
//import java.util.Arrays;
//
//public class DynamicCompiler {
//
//    public static void main(String[] args) {
//        compiler("1");
//        compiler("2");
//        compiler("3");
//    }
//
//    public static Class<?> compilerCmp() {
//        String root = "ivy-system\\target\\classes";
//        String className = "TestCmp";
//        String packageName = "com.ming.liteflow.cmp.node";
//        String packagePath = "com\\ming\\liteflow\\cmp\\node";
//        String rootPath = "ivy-dynamic-class\\"+packagePath;
//        String forName = packageName+"."+className;
//        String sourceCode = "import com.yomahub.liteflow.annotation.LiteflowComponent;\n" +
//                "import com.yomahub.liteflow.core.NodeComponent;\n" +
//                "@LiteflowComponent(\"TestCmp\")\n" +
//                "public class TestCmp extends NodeComponent {\n" +
//                "\t@Override\n" +
//                "\tpublic void process() {\n" +
//                "\t\tSystem.out.println(\"TestCmp executed!\");\n" +
//                "\t}\n" +
//                "\n" +
//                "}";
//        //String methodName = "sayHello";
//        Class<?> dynamicClass = null;
//        try {
//            // 检查并创建根路径
//            File rootDir = new File(rootPath);
//            if (!rootDir.exists()) {
//                if (!rootDir.mkdirs()) {
//                    System.err.println("无法创建动态class目录");
//                    System.exit(1);
//                }
//            }
//
//            // 确保输出目录存在
//            File outputDir = new File(root);
//
//            // 保存源码到文件
//            File sourceFile = new File(rootDir, className + ".java");
//            FileWriter writer = new FileWriter(sourceFile);
//            writer.write("package " + packageName + "; " + sourceCode);
//            writer.close();
//
//            // 获取系统编译器
//            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//
//            // 获取文件管理器
//            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
//
//            // 获取编译单元
//            Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile));
//
//            // 设置编译参数
//            Iterable<String> options = Arrays.asList("-d", outputDir.getAbsolutePath());
//
//            // 创建编译任务
//            JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, null, options, null, compilationUnits);
//
//            // 执行编译任务
//            boolean success = task.call();
//
//            if (success) {
//                // 加载编译后的类
//                URLClassLoader classLoader = new URLClassLoader(new URL[]{new File(root).toURI().toURL()});
//                dynamicClass = Class.forName(forName, true, classLoader);
//
//                // 创建实例并调用方法
//                //Object instance = dynamicHelloClass.getDeclaredConstructor().newInstance();
//                //dynamicHelloClass.getMethod(methodName).invoke(instance);
//            }
//
//            // 关闭文件管理器
//            fileManager.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return dynamicClass;
//    }
//
//
//    public static void compiler(String s) {
//        String root = "ivy-dynamic-class";
//        String className = "DynamicHello";
//        String packageName = "com.ming.dynamic";
//        String packagePath = "com\\ming\\dynamic";
//        String rootPath = "ivy-dynamic-class\\"+packagePath;
//        String forName = "com.ming.dynamic."+className;
//        String sourceCode = "public class DynamicHello { public void sayHello() { System.out.println(\"Dynamic Hello"+s+"!\"); }}";
//        String methodName = "sayHello";
//
//        try {
//            // 检查并创建根路径
//            File rootDir = new File(rootPath);
//            if (!rootDir.exists()) {
//                if (!rootDir.mkdirs()) {
//                    System.err.println("无法创建动态class目录");
//                    System.exit(1);
//                }
//            }
//
//            // 确保输出目录存在
//            File outputDir = new File(root);
//
//            // 保存源码到文件
//            File sourceFile = new File(rootDir, className + ".java");
//            FileWriter writer = new FileWriter(sourceFile);
//            writer.write("package " + packageName + "; " + sourceCode);
//            writer.close();
//
//            // 获取系统编译器
//            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//
//            // 获取文件管理器
//            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
//
//            // 获取编译单元
//            Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile));
//
//            // 设置编译参数
//            Iterable<String> options = Arrays.asList("-d", outputDir.getAbsolutePath());
//
//            // 创建编译任务
//            JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, null, options, null, compilationUnits);
//
//            // 执行编译任务
//            boolean success = task.call();
//
//            if (success) {
//                // 加载编译后的类
//                URLClassLoader classLoader = new URLClassLoader(new URL[]{new File(root).toURI().toURL()});
//                Class<?> dynamicHelloClass = Class.forName(forName, true, classLoader);
//
//                // 创建实例并调用方法
//                Object instance = dynamicHelloClass.getDeclaredConstructor().newInstance();
//                dynamicHelloClass.getMethod(methodName).invoke(instance);
//            }
//
//            // 关闭文件管理器
//            fileManager.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}