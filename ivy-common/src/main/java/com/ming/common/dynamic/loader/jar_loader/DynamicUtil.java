package com.ming.common.dynamic.loader.jar_loader;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DynamicUtil {


    public static Set<String> getImportList(String javaSourceCode) {
        Pattern importPattern = Pattern.compile("import\\s+([^;]+);");
        Matcher matcher = importPattern.matcher(javaSourceCode);
        Set<String> list = new HashSet<>();
        while (matcher.find()) {
            String importStatement = matcher.group(1).trim();
            System.out.println("Imported class: " + importStatement);
            list.add(importStatement);
        }
        return list;
    }

}
