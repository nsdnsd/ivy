package com.ming.system;

import com.ming.common.generate.template.enable.EnableGenerate;
import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@EnableGenerate(basePackages = {"com.ming"})
@ComponentScans(@ComponentScan(basePackages = {"com"}))
@SpringBootApplication(exclude = {RedissonAutoConfiguration.class})
public class IvyApplication {

    public static void main(String[] args) {
        SpringApplication.run(IvyApplication.class, args);
        // 创建一个关闭钩子
        Thread shutdownHook = new Thread(() -> {
            // 在 JVM 即将关闭时执行的操作
            // destroyClass(SpringBeanUtil.getBean("sqlManager"));
        });
        // 将关闭钩子添加到 JVM
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        // 模拟 JVM 关闭
        // simulateJvmShutdown();
    }

    private static void simulateJvmShutdown() {
        // 模拟 JVM 关闭前的清理工作
        // 例如：关闭数据库连接、保存状态等

        // JVM 关闭
        System.exit(0);
    }

}
